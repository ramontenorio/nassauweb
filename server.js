const express = require('express');
const app     = express();
const port    = process.env.PORT || 8080;
const hbs     = require('hbs');
const bodyParser = require('body-parser');
const livroRoutes = require('./routes/livro');
const mongoose  = require('mongoose');
const flash   = require('connect-flash');
const session = require('express-session');
const methodOverride = require('method-override');

// Database connection
mongoose.connect('mongodb://nassauweb_user:nassauweb123@ds159527.mlab.com:59527/nassauweb');
mongoose.connection
  .once('open', () => {
    console.log('Tudo tranquilo!');
  })
  .on('error', (error) => {
    console.warn(`Ocorreu o seguinte erro: ${error}`);  
  });

app.use(methodOverride('_method'));

mongoose.Promise = global.Promise;

// Pasta Pública
app.use(express.static(__dirname + '/public'));

// Template engine - Handlebars
app.set('view engine', 'hbs');

// Configuração das páginas parciais
hbs.registerPartials(__dirname + '/views/partials');

// Configuração do bodyParser
app.use(bodyParser.urlencoded({ extended: true }));

app.use(flash());

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 60000 }
}));

app.use(function(req, res, next) {
  res.locals.success  = req.flash('success');
  res.locals.error    = req.flash('error');
  res.locals.info     = req.flash('info');
  
  next();
});


// Rota de Livros
app.use('/livros', livroRoutes);

app.get('/', (req, res) => {
//   res.send('Epaaaaa!!');
  res.render('index');
});

// app.get('/livros', (req, res) => {
//   res.send('Livroosss');
// });

app.listen(port, () => {
  console.log(`The server is running on port ${port}.`);
});